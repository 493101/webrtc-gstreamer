FROM ubuntu:20.04 as builder

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    wget \
    libgstreamer1.0-dev \
    libgstreamer-plugins-bad1.0-dev  \
    libgstreamer-plugins-base1.0-dev \
    libgstreamer-plugins-good1.0-dev

RUN wget -O go.tgz https://go.dev/dl/go1.18.1.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go.tgz
ENV PATH=$PATH:/usr/local/go/bin

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download
COPY . .

RUN go build -o ./webrtc-gstreamer cmd/main.go


FROM nvidia/cuda:11.4.0-base-ubuntu20.04
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    gstreamer1.0-plugins-bad  \
    gstreamer1.0-plugins-ugly  \
    gstreamer1.0-libav  \
    gstreamer1.0-plugins-good  \
    gstreamer1.0-pulseaudio \
    gstreamer1.0-tools \
    ladspa-sdk  \
    libopus-dev \
    libpulse-dev \
    swh-plugins

COPY --from=builder /app/webrtc-gstreamer /app

ENTRYPOINT ["./app"]

CMD ["-addr", "localhost:9000", "-session", "ion", "-video-src", "ximagesrc"]

